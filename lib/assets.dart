import 'dart:async';
import 'dart:io';

import 'package:flutter/services.dart' show rootBundle;
import 'package:path_provider/path_provider.dart';

Future<String> loadConfig() async {
  return await rootBundle.loadString('assets/config.json');
}
