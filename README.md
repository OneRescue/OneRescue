
<div align="center">
  <h1>OneRescue</h1>
  <p>
    <strong>
      One app for carrying out rescue operations.
    </strong>
  </p>

[![Release](https://img.shields.io/badge/release-none-red)](https://codeberg.org/OneRescue/OneRescue/releases)
[![License](https://img.shields.io/badge/GPL-3.0_only-green)](https://opensource.org/licenses/GPL-3.0)
[![Version](https://img.shields.io/badge/dart-2.17-blue.svg)]()
<br />
[![Chat on Matrix](https://img.shields.io/badge/chat-on_matrix-51bb9c)](https://matrix.to/#/#onerescue:matrix.org)
  
</div>

## Table of content

- [About](#about)
  - [Planned functionalities](#planned-functionalities)
- [Assumptions](#assumptions)
- [Screenshots](#screenshots)
- [Operating systems](#operating-systems)
- [Built with](#built-with)
  - [Building](#building)
- [TODO](#todo)
- [Donation](#donation)
  - [Cryptocurrencies](#cryptocurrencies)
- [Licensing](#licensing)

# About

> About the project

<strong>The aim of the project is to create one application for carrying out rescue operations.</strong>

What will make this app stand out from other rescue apps?

## Planned functionalities

- Operation without a central server
- Voice communication with other participants in the rescue operation
- Send messages to other rescuers
- Having an on-demand connection to the missing persons database
- Integration with drones
- Marking the searched area on the map
- Having a storage shared between rescuers with photo and information about the missing person 
- Show the position of other rescuers in real time
# Assumptions

- Free and open-source
- Without profiling users and collecting statistics
- Strong security and E2EE
- Newest technologies
- Modern graphic look
# Screenshots
![main view](screenshots/main.png "Main view"))
# Operating systems

> App tests on operating systems

| Desktop operating system | Tested | Works |
| ----------------------- | ------ | ----- |
| Debian Bullseye         |  No    |        |

| Operating system in Qubes OS templates| Tested | Works |
| ----------------------- | ------ | ----- |
| Debian Bullseye         | Yes    | Yes   |

| Mobile operating system | Tested | Works |
| -------------------------- | ------ | ----- |
| Android 11                 |   No   |       |

# Built with

- Dart 2.19
- Dart Flutter 3.1

## Building

> How to build

```
git clone https://codeberg.org/OneRescue/OneRescue
flutter run
```

# TODO

# Donation

## Cryptocurrencies

- Pirate Chain (ARRR):

  >

- Beam:

  >

- Monero (XMR):

  >

- Dash:

  >

- Bitcoin Cash (BCH):

  >

- Ethereum (ETH):

  >

- Dogecoin (DOGE):

  >

- Litecoin (LTC):

  >

- Bitcoin (BTC):
  >

# Licensing

GPL-3.0 only
